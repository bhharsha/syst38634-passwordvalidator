package password;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * @author bhatt    Name : Harshal Bhatt      Student ID : 991546101
 *
 */
public class PasswordValidatorTest {
	
	
	@Test
	public void testGetIsValidLengthRegular() {
		boolean isValidLength = PasswordValidator.isValidLength("HelloThere");
		assertTrue("Invalid password length.", isValidLength);
	}

	@Test 
	public void testGetIsValidLengthException() {
		boolean isValidLength = PasswordValidator.isValidLength("Hello");
		assertFalse("Invalid password length.", isValidLength);
	}
	
	@Test 
	public void testGetIsValidLengthBoundaryOut() {
		boolean isValidLength = PasswordValidator.isValidLength("HelloHi");
		assertFalse("Invalid password length.", isValidLength);
	}

	@Test 
	public void testGetIsValidLengthBoundaryIn() {
		boolean isValidLength = PasswordValidator.isValidLength("HelloHi2");
		assertTrue("Invalid password length.", isValidLength);
	}
	//------------------------------------------------------------
	
	@Test
	public void testGetHasValidDigitCountRegular() {
		boolean hasValidDigitCount = PasswordValidator.hasValidDigitCount(9);
		assertTrue("Invalid password Digit Count.", hasValidDigitCount);
	}

	@Test 
	public void testGetHasValidDigitCountException() {
		boolean hasValidDigitCount = PasswordValidator.hasValidDigitCount(2);
		assertFalse("Invalid password Digit Count.", hasValidDigitCount);
	}
	
	@Test 
	public void testGetHasValidDigitCountBoundaryOut() {
		boolean hasValidDigitCount = PasswordValidator.hasValidDigitCount(6);
		assertFalse("Invalid password Digit Count.", hasValidDigitCount);
	}

	@Test 
	public void testGetHasValidDigitCountBoundaryIn() {
		boolean hasValidDigitCount = PasswordValidator.hasValidDigitCount(8);
		assertTrue("Invalid password Digit Count.", hasValidDigitCount);
	}
	//-----------------------------------------------------------------------------
	@Test 
	public void testGetIsValidLengthExceptionSpaces() {
		boolean isValidLength = PasswordValidator.isValidLength("Hello World");
		assertFalse("Invalid password length.", isValidLength);
	}
	
	@Test 
	public void testGetIsValidLengthNull() {
		boolean isValidLength = PasswordValidator.isValidLength(null);
		assertFalse("Invalid password length.", isValidLength);
	}
}
