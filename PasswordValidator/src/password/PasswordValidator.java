package password;

/**
 * @author bhatt    Name : Harshal Bhatt      Student ID : 991546101
 * Assume spaces are not valid characters for the purpose of calculating length.
 *
 */
public class PasswordValidator {

	public static boolean isValidLength(String password) {
		
		return (password != null && !password.contains(" ") && password.length() >= 8);
		
	}
	
public static boolean hasValidDigitCount(Integer password) {
		
		return (password != null);
		
	}
}
